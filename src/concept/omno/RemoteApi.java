package concept.omno;

import concept.utility.JsonFunction;
import concept.utility.RestApi;
import org.json.simple.JSONObject;

public class RemoteApi extends RestApi {

    final ApplicationContext applicationContext;

    String password;

    public RemoteApi(ApplicationContext applicationContext, String hostProtocolString, String hostNameString, String hostPortString, String password) {
        super(hostProtocolString, hostNameString, hostPortString, "api");

        this.applicationContext = applicationContext;

        this.password = password;
    }

    public State getState() {
        JSONObject jsonObject = new JSONObject();

        if (password != null) {
            JsonFunction.put(jsonObject, "password", password);
        }

        JsonFunction.put(jsonObject, "service", "platform");
        JsonFunction.put(jsonObject, "request", "state");

        JSONObject response;

        try {
            response = jsonObjectHttpApiRaw(false, jsonObject);
        } catch (Exception e) {
            return null;
        }

        return new State(applicationContext, JsonFunction.getJSONObject(response, "state", null));
    }
}
