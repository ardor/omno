package concept.omno;

import concept.utility.JsonFunction;
import org.json.simple.JSONObject;
import sun.misc.Signal;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) throws Exception {
        HashMap<String, String> applicationResult = new HashMap<>();
        ApplicationContext applicationContext = null;
        Httpd httpd;

        testArguments:
        {
            if (args.length > 1) {
                applicationResult.put("errorDescription", "usage :\nconfiguration\n");
                break testArguments;
            }

            String configurationFileName = "configuration.json";

            if (args.length == 1) {
                configurationFileName = args[0];
            }

            applicationContext = new ApplicationContext(configurationFileName);
        }

        if (applicationContext != null && applicationContext.isConfigured) {
            Thread applicationContextThread = new Thread(applicationContext);

            httpd = new Httpd(applicationContext, applicationContext.apiHost, applicationContext.apiPort);
            Thread httpdThread = new Thread(httpd);


            applicationContextThread.start();

            httpdThread.start();

            final ApplicationContext finalApplicationContext = applicationContext;

            Signal.handle(new Signal("INT"), signal -> {

                finalApplicationContext.logInfoMessage("shutting down...");

                httpd.stop();
                finalApplicationContext.stop();
            });

            httpdThread.join();

            applicationContextThread.join();
        }

        JSONObject applicationResultJson = new JSONObject();

        for(String key : applicationResult.keySet()) {
            JsonFunction.put(applicationResultJson, key, applicationResult.get(key));
        }

        if (applicationResultJson.size() != 0) {
            System.out.println(applicationResultJson.toJSONString());
        }

        System.exit(0);
    }
}
